//
// DZWAnalyticsSDK+Public.h
//  DZWAnalyticsSDK
//
//  Created by guoqin yan on 2022/9/2.
//

#import <Foundation/Foundation.h>
#import "DZWAConstants.h"

@class DZWAConfigOptions;

NS_ASSUME_NONNULL_BEGIN

/**
 * @class
 * DZWAnalyticsSDK 类
 *
 * @abstract
 * 在 SDK 中嵌入 DZWAnalytics 的 SDK 并进行使用的主要 API
 *
 * @discussion
 * 使用 DZWAnalyticsSDK 类来跟踪用户行为，并且把数据发给所指定的 DZWAnalytics 的服务。
 */
@interface DZWAnalyticsSDK : NSObject

/**
 * @property
 *
 * @abstract
 * 获取用户的唯一用户标识
 */
@property (atomic, readonly, copy) NSString *distinctId;


#pragma mark- init instance
/**
 通过配置参数，配置大众网分析 SDK

 此方法调用必须符合以下条件：
     1、必须在应用启动时调用，即在 application:didFinishLaunchingWithOptions: 中调用，
     2、必须在主线线程中调用
     3、必须在 SDK 其他方法调用之前调用
 如果不符合上述条件，存在丢失 open 事件

 @param configOptions 参数配置
 */
+ (void)startWithConfigOptions:(nonnull DZWAConfigOptions *)configOptions NS_SWIFT_NAME(start(configOptions:));

/**
 * @abstract
 * 返回之前所初始化好的单例
 *
 * @discussion
 * 调用这个方法之前，必须先调用 startWithConfigOptions: 这个方法
 *
 * @return 返回的单例
 */
+ (DZWAnalyticsSDK * _Nullable)sharedInstance;

/// 禁用 SDK。调用后，SDK 将不采集事件，不发送网络请求
+ (void)disableSDK;

/// 开启 SDK。如果之前 SDK 是禁止状态，调用后将恢复数据采集功能
+ (void)enableSDK;


#pragma mark track event
/**
 * @abstract
 * 调用 track 接口，追踪一个带有属性的 event
 *
 * @discussion
 * propertyDict 是一个 Map。
 * 其中的 key 是 Property 的名称，必须是 NSString
 * value 则是 Property 的内容，只支持 NSString、NSNumber、NSSet、NSArray、NSDate 这些类型
 * 特别的，NSSet 或者 NSArray 类型的 value 中目前只支持其中的元素是 NSString
 *
 * @param event             event的名称
 * @param propertyDict     event的属性
 * event :浏览稿件(news)、点赞稿件(praise)、分享稿件(share)、评论稿件(comment)、用户注册(reg)、用户登录(login)、收藏稿件(collect)、浏览专题(zt)、浏览直播(live)
 * propertyDict: @{@"eventid":@"243254546",@"url":@"文章的h5链接",@"key1":@"value1",@"key2":@"value2"}，其中eventid必填，为文章ID或用户ID（用户注册登录）url有就填。
 */
- (void)track:(NSString *)event withProperties:(nullable NSDictionary *)propertyDict;

/**
 * @abstract
 * 得到 SDK 的版本
 *
 * @return SDK 的版本
 */
- (NSString *)libVersion;

/**
 * 判断是否为符合要求的 openURL

 * @param url 打开的 URL
 * @return YES/NO
 */
- (BOOL)canHandleURL:(NSURL *)url API_UNAVAILABLE(macos);

/**
 * @abstract
 * 处理 url scheme 跳转打开 App
 *
 * @param url 打开本 app 的回调的 url
 */
- (BOOL)handleSchemeUrl:(NSURL *)url API_UNAVAILABLE(macos);

/**
 * @abstract
 * log 功能开关
 *
 * @discussion
 * 根据需要决定是否开启 SDK log , DZWAnalyticsDebugOff 模式默认关闭 log
 * DZWAnalyticsDebugOnly  DZWAnalyticsDebugAndTrack 模式默认开启log
 *
 * @param enabelLog YES/NO
 */
- (void)enableLog:(BOOL)enabelLog;

#pragma mark - utils
//以下方法，不需要用户去调用
/**
 * @abstract
 * 强制试图把数据传到对应的 DZWAnalytics 服务器上
 *
 * @discussion
 * 主动调用 flush 接口，则不论 flushInterval 和 flushBulkSize 限制条件是否满足，都尝试向服务器上传一次数据
 */
- (void)flush;

/**
 * @abstract
 * 注册属性插件
 *
 * @param plugin 属性插件对象
 */
- (void)registerPropertyPlugin:(id)plugin;

/**
 开始事件计时

 @discussion
 若需要统计某个事件的持续时间，先在事件开始时调用 trackTimerStart:"Event" 记录事件开始时间，该方法并不会真正发送事件；
 随后在事件结束时，调用 trackTimerEnd:"Event" withProperties:properties，
 SDK 会追踪 "Event" 事件，并自动将事件持续时间记录在事件属性 "event_duration" 中，时间单位为秒。

 @param event 事件名称
 @return 返回计时事件的 eventId，用于交叉计时场景。普通计时可忽略
 */
- (nullable NSString *)trackTimerStart:(NSString *)event;

@end



NS_ASSUME_NONNULL_END
