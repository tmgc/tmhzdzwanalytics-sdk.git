//
// DZWAConstants.h
//  DZWAnalyticsSDK
//
//  Created by guoqin yan on 2022/9/2.
//

#import <Foundation/Foundation.h>


#pragma mark - typedef
/**
 * @abstract
 * Debug 模式，用于检验数据导入是否正确。该模式下，事件会逐条实时发送到 DZWAnalytics，并根据返回值检查
 * 数据导入是否正确。
 *
 
 * Debug模式有三种选项:
 *   DZWAnalyticsDebugOff - 关闭 DEBUG 模式
 *   DZWAnalyticsDebugOnly - 打开 DEBUG 模式，但该模式下发送的数据仅用于调试，不进行数据导入
 *   DZWAnalyticsDebugAndTrack - 打开 DEBUG 模式，并将数据导入到 DZWAnalytics 中
 */
typedef NS_ENUM(NSInteger, DZWAnalyticsDebugMode) {
    DZWAnalyticsDebugOff,
    DZWAnalyticsDebugOnly,
    DZWAnalyticsDebugAndTrack,
};

/**
 * @abstract
 * TrackTimer 接口的时间单位。调用该接口时，传入时间单位，可以设置 event_duration 属性的时间单位。
 *
 * @discuss
 * 时间单位有以下选项：
 *   DZWAnalyticsTimeUnitMilliseconds - 毫秒
 *   DZWAnalyticsTimeUnitSeconds - 秒
 *   DZWAnalyticsTimeUnitMinutes - 分钟
 *   DZWAnalyticsTimeUnitHours - 小时
 */
typedef NS_ENUM(NSInteger, DZWAnalyticsTimeUnit) {
    DZWAnalyticsTimeUnitMilliseconds,
    DZWAnalyticsTimeUnitSeconds,
    DZWAnalyticsTimeUnitMinutes,
    DZWAnalyticsTimeUnitHours
};


/**
 * @abstract
 * AutoTrack 中的事件类型
 *
 * @discussion
 *   DZWAnalyticsEventTypeAppStart - $AppStart
 *   DZWAnalyticsEventTypeAppEnd - $AppEnd
 *   DZWAnalyticsEventTypeAppClick - $AppClick
 *   DZWAnalyticsEventTypeAppViewScreen - $AppViewScreen
 */
typedef NS_OPTIONS(NSInteger, DZWAnalyticsAutoTrackEventType) {
    DZWAnalyticsEventTypeNone      = 0,
    DZWAnalyticsEventTypeAppStart      = 1 << 0,
    DZWAnalyticsEventTypeAppEnd        = 1 << 1,
    DZWAnalyticsEventTypeAppClick      = 1 << 2,
    DZWAnalyticsEventTypeAppViewScreen = 1 << 3,
};

/**
 * @abstract
 * 网络类型
 *
 * @discussion
 *   DZWAnalyticsNetworkTypeNONE - NULL
 *   DZWAnalyticsNetworkType2G - 2G
 *   DZWAnalyticsNetworkType3G - 3G
 *   DZWAnalyticsNetworkType4G - 4G
 *   DZWAnalyticsNetworkTypeWIFI - WIFI
 *   DZWAnalyticsNetworkTypeALL - ALL
 *   DZWAnalyticsNetworkType5G - 5G
 */
typedef NS_OPTIONS(NSInteger, DZWAnalyticsNetworkType) {
    DZWAnalyticsNetworkTypeNONE         = 0,
    DZWAnalyticsNetworkType2G API_UNAVAILABLE(macos)    = 1 << 0,
    DZWAnalyticsNetworkType3G API_UNAVAILABLE(macos)    = 1 << 1,
    DZWAnalyticsNetworkType4G API_UNAVAILABLE(macos)    = 1 << 2,
    DZWAnalyticsNetworkTypeWIFI     = 1 << 3,
    DZWAnalyticsNetworkTypeALL      = 0xFF,
#ifdef __IPHONE_14_1
    DZWAnalyticsNetworkType5G API_UNAVAILABLE(macos)   = 1 << 4
#endif
};

/// 事件类型
typedef NS_OPTIONS(NSUInteger, DZWAEventType) {
    DZWAEventTypeTrack = 1 << 0,
    DZWAEventTypeSignup = 1 << 1,
    DZWAEventTypeBind = 1 << 2,
    DZWAEventTypeUnbind = 1 << 3,

    DZWAEventTypeProfileSet = 1 << 4,
    DZWAEventTypeProfileSetOnce = 1 << 5,
    DZWAEventTypeProfileUnset = 1 << 6,
    DZWAEventTypeProfileDelete = 1 << 7,
    DZWAEventTypeProfileAppend = 1 << 8,
    DZWAEventTypeIncrement = 1 << 9,

    DZWAEventTypeItemSet = 1 << 10,
    DZWAEventTypeItemDelete = 1 << 11,

    DZWAEventTypeDefault = 0xF,
    DZWAEventTypeAll = 0xFFFFFFFF,
};
