#
#  Be sure to run `pod spec lint TMSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  s.name         = "TMHZDZWAnalyticsSDK"
  s.version      = "0.0.1"
  s.summary      = "TM 菏泽统计组件"
  s.homepage     = "https://www.360tianma.com"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "qiuyong" => "qiuyong@360tianma.com" }
  s.platform     = :ios
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://gitee.com/tmgc/tmhzdzwanalytics-sdk.git", :tag => s.version }
  s.source_files = 'TMFramework/DZWAnalyticsSDK.framework/Headers/*.{h}'
  s.ios.vendored_frameworks = 'TMFramework/DZWAnalyticsSDK.framework'
  s.resources = "TMFramework/DZWAnalyticsSDK.bundle"
  s.requires_arc = true

s.xcconfig = {
  'VALID_ARCHS' =>  'armv7 x86_64 arm64',
}
 
end
 
